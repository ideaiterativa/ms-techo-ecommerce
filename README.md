
# Techno MS

Este es el proyecto de ecommerce creado para el curso de React JS de Coderhouse



## Tech Stack

**Client:** React JS, Bootstrap

**Server:** Node

**Backend:** Firebase (for data storage)


## Authors

- [@marianosciuto](https://gitlab.com/ideaiterativa)


## Installation

Descargar/clonar el proyecto localmente

```bash
  git clone https://gitlab.com/ideaiterativa/ms-techo-ecommerce.git
```

Configurar las variables de entorno descriptas en la siguiente sección [Enviroment variables]. Completar dichas variables en un archivo .env dentro de la carpeta raíz del proyecto
```bash
  /.env
```

Correr el proyecto

```bash
  cd proyect-folder
  npm install
  npm run dev
```

    
## Environment Variables

El proyecto cuenta con una serie de variables de entorno que deben ser configuradas previas a la ejecucción del proyecto. Estas variables son provistas por Firebase para la conexión a la base de datos

`API_KEY`

`AUTH_DOMAIN`

`PROJECT_ID`

`STORAGE_BUCKET`

`MESSAGING_SENDER_ID`

`APP_ID`


## Demo

Existe una demo del proyecto la cual se encuentra deployada en el siguiente [link](https://ms-techo-ecommerce.vercel.app/) 


## 🚀 About Me
Soy Mariano Sciuto. Ingeniero en Sistemas, Java full stack developer & Trainer.

Podes encontrarme en[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/marianosciuto/)
