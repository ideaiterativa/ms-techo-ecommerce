import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCwbPCCVw0Ie2L9eMO4n8HbhLSL-HMhCMk",
  authDomain: "my-coderhouse-ecommerce.firebaseapp.com",
  projectId: "my-coderhouse-ecommerce",
  storageBucket: "my-coderhouse-ecommerce.appspot.com",
  messagingSenderId: "448384058841",
  appId: "1:448384058841:web:402808def4eca7640bc5c8"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const fs_db = getFirestore(app);