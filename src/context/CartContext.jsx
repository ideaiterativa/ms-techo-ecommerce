import { createContext, useContext, useState } from "react";

const cartContext = createContext();

const {Provider} = cartContext;

export const useCartContext = () => useContext(cartContext);

const CartProvider = ({children}) => {
    const [cart, setCart] = useState(new Map());
    const [totalProducts, setTotalProducts] = useState(0);
    const [totalAmount, setTotalAmount] = useState(0);

    const addProduct = (item, cant) => {
        let cantidad;

        if(yaAgregado(item)) {
            const prod = cart.get(item.id);
            cantidad = prod.cantidad + cant;
        } else {
            cantidad = cant;
        }
        
        cart.set(item.id, {item, cantidad});
        setTotalProducts(totalProducts + cant);
        setTotalAmount(totalAmount + item.price * cant);
    }
    
    const removeProduct = (id) => {
        const prod = cart.get(id);

        cart.delete(id);
        setTotalProducts(totalProducts - prod.cantidad);
        setTotalAmount(totalAmount - prod.item.price * prod.cantidad);
    }

    const clearCart = () => {
        setCart(new Map());
        setTotalProducts(0);
        setTotalAmount(0);
    }
    
    const yaAgregado = (item) => {
        return cart.has(item.id);
    }

    return <Provider value={{addProduct, removeProduct, clearCart, cart, totalProducts, totalAmount}}>{children}</Provider>
}

export default CartProvider;