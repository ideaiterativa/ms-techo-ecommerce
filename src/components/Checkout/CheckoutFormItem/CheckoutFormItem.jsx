const CheckoutFormItem = ({name, price, cant}) => {
    return (
        <li className="list-group-item d-flex justify-content-between lh-sm">
            <div>
                <h6 className="my-0">{name}</h6>
                <small className="text-muted">x&nbsp;{cant}</small>
            </div>
            <span className="text-muted">$&nbsp;{price * cant}</span>
        </li>
    );
}

export default CheckoutFormItem