const CheckoutConfirmation = ({orderID}) => {
    return(
        <section className="py-5">
            <div className="container px-4 px-lg-5 mt-5 text-center">
                <h1>¡Tu compra fue confirmada!</h1>
                <div class="alert alert-success" role="alert">
                    El identificador de tu compra es el #{orderID}
                </div>
            </div>
        </section>
    );
}

export default CheckoutConfirmation