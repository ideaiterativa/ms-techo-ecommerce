import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import { useCartContext } from "../../../context/CartContext";
import { useState } from "react";
import { fs_db } from "../../../firebase/config";
import CheckoutFormItem from "../CheckoutFormItem/CheckoutFormItem";
import './CheckoutForm.css'
import Loading from "../../Loading/Loading";

const CheckoutForm = ({onConfirmation}) => {
    const {cart, clearCart, totalProducts, totalAmount} = useCartContext();
    const products = [...cart.entries()];
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [processing, setProcessing] = useState(false);

    const placeOrder = async (e) => {
        e.preventDefault();
        
        setProcessing(true);
        const items = products.map(([key, value]) => {
            return {id: key, sku: value.item.sku, name: value.item.name, price: value.item.price, q: value.cantidad}
        });
        
        let order = {
            buyer: {
                name: name,
                phone: phone,
                email: email
            },
            total: totalAmount,
            items: items,
            date: serverTimestamp()
        };
        
        const ordersCollection = collection(fs_db, "orders");
        addDoc(ordersCollection, order).then((orderRef) => {
            clearCart();
            setProcessing(false);
            onConfirmation(orderRef.id);
        }).catch((error) => {
            console.log("No se pudo generar la orden: " + error);
        });
    }


    return (
        <section className="py-5">
            <div className="container px-4 px-lg-5 mt-5 text-center">
                <h2>Finalizar Compra</h2>
                <p>Tu compra está casi lista! Completá tus datos y presioná el botón de Finalizar Compra</p>
            </div>
            
            {
                ((processing) ?
                    <Loading message={"Estamos procesando tu pedido..."} />
                :
                    <form className="needs-validation" onSubmit={placeOrder}>
                        <div className="container spaced-control">
                            <div className="row g-5">
                                <div className="col-md-7 col-lg-8">
                                    <h4 className="mb-3 text-secondary">Tus datos</h4>
                                    <div className="row g-3">
                                        <div className="col-sm-6">
                                            <label for="name" className="form-label">Tu nombre</label>
                                            <div className="input-group has-validation">
                                                <span className="input-group-text">
                                                    <i className="bi bi-person-fill"></i>
                                                </span>
                                                <input id="name" name="name" type="text" className="form-control" placeholder="Ingresá tu nombre" onChange={(e) => setName(e.target.value)} required />
                                                <div className="invalid-feedback">El nombre es requerido</div>
                                            </div>
                                        </div>

                                        <div className="col-sm-6">
                                            <label for="phone" className="form-label">Tu teléfono</label>
                                            <div className="input-group has-validation">
                                                <span className="input-group-text">
                                                    <i className="bi bi-telephone-fill"></i>
                                                </span>
                                                <input id="phone" name="phone" type="text" className="form-control" placeholder="Ingresá tu teléfono" onChange={(e) => setPhone(e.target.value)} required />
                                                <div className="invalid-feedback">El teléfono es requerido</div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <label for="email" className="form-label">Un email</label>
                                            <div className="input-group has-validation">
                                                <span className="input-group-text">
                                                    <i class="bi bi-envelope-open-fill"></i>
                                                </span>
                                                <input id="email" name="email" type="email" className="form-control" placeholder="Ingresá tu dirección de email" onChange={(e) => setEmail(e.target.value)} required />
                                                <div className="invalid-feedback">El email es requerido</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-5 col-lg-4 order-md-last">
                                    <h4 className="d-flex justify-content-between align-items-center mb-3">
                                        <span className="text-secondary">Tu compra</span>
                                        <span className="badge bg-secondary rounded-pill">{totalProducts}</span>
                                    </h4>
                                    <ul className="list-group mb-3">
                                        {
                                            products.map(([key, value]) => {
                                                return <CheckoutFormItem key={key} name={value.item.name} price={value.item.price} cant={value.cantidad} />

                                            })
                                        }
                            
                                        <li className="list-group-item d-flex justify-content-between">
                                            <span><strong>TOTAL</strong></span>
                                            <strong>$&nbsp;{totalAmount}</strong>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="row">
                                <button className="w-100 btn btn-primary btn-lg spaced-control" type="submit">Finalizar Compra</button>
                            </div>
                        </div>
                    </form>
                )
            }
        </section>
    )
}

export default CheckoutForm;