import { useState } from "react";
import CheckoutConfirmation from "./CheckoutConfirmation/CheckoutConfirmation";
import CheckoutForm from "./CheckoutForm/CheckoutForm";

const Checkout = () => {
    const [orderId, setOrderId] = useState("");

    return(
        (orderId == "" ? <CheckoutForm onConfirmation={setOrderId} /> : <CheckoutConfirmation orderID={orderId} />)
    )
}

export default Checkout