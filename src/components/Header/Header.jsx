import './Header.css'

function Header() {
    return (
        <header className="bg-dark py-5 blured-bg">
            <div className="container px-4 px-lg-5 my-5">
                <div className="text-center text-white bg-text">
                    <h1 className="display-4 fw-bolder">Techno MS</h1>
                    <p className="badge text-bg-secondary">La tecnología a un click de distancia</p>
                </div>
            </div>
        </header>
    );
}

export default Header