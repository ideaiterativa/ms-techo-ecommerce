import { useEffect, useState } from "react";
import Loading from "../Loading/Loading";
import { useCartContext } from "../../context/CartContext";
import CartItemList from "../CartItemList/CartItemList";
import EmptyCartButton from "../Buttons/EmptyCartButton";
import CheckoutButton from "../Buttons/CheckoutButton";

const CartListContainer = () =>{
    const {cart, totalAmount, removeProduct, clearCart} = useCartContext();
    const products = [...cart.entries()];
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            setLoading(false);
          }, 2000);
        
        return () => {

        }
    }, [cart]);


    const deleteItem = (id) =>{
        removeProduct(id);
    }

    const emptyCart = () => {
        clearCart();
    }

    return(
        (loading ? <Loading message="Cargando carrito de compras..." /> :
        <section className="py-5">
            <div className="container px-4 px-lg-5 mt-5">
                <div className="row">
                    <div className="col-md-6">
                        <h2 className="text-secondary fw-bolder">Tu carrito de compras</h2>
                    </div>
                    {
                        (cart.size != 0 ? 
                            <div className="col-md-6 text-end">
                                <EmptyCartButton onEmptyCart={emptyCart} />
                            </div> : ""
                        )
                    }
                </div>
                {
                    (cart.size == 0 ? <p>Aún no hay productos seleccionados</p> :
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>SKU</th>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th className="text-center">Cantidad</th>
                                    <th className="text-center">Sub total</th>
                                    <th className="text-end">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    products.map(([key, value]) => {
                                        return <CartItemList key={value.item.sku} item={value.item} cantidad={value.cantidad} onClick={deleteItem} />
                                    })
                                }
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td className="text-end" colSpan={6}>
                                        <h3 className="text-secondary fw-bolder">TOTAL DE LA COMPRA: $&nbsp;{totalAmount}</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="text-end" colSpan={6}>
                                        <CheckoutButton />
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    
                    )
                }
            </div>
        </section>
        )
    );
}

export default CartListContainer;