import { Link } from "react-router-dom";
import ItemStar from "../ItemStar/ItemStar";

function Item({id, name, countStars, price, imgSource}) {
    return(
        <div className="col mb-5">
            <div className="card h-100">
                <img className="card-img-top" src={"/img/products/" + imgSource} alt={name} />
                    <div className="card-body p-4">
                        <div className="text-center">
                            <h5 className="fw-bolder">{name}</h5>
                            <ItemStar starsCount={countStars} />
                            $ {price}
                        </div>
                    </div>
                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div className="text-center"><Link to={"/item/" + id} className="btn btn-outline-dark mt-auto">Ver detalle</Link></div>
                </div>
            </div>
        </div>
    );
}

export default Item;