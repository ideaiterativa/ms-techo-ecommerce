import CartWidget from "../CartWidget/CartWidget";
import NavBarBtn from "./NavBarBtn";
import ch_logo  from '../../assets/coderhouse.png'
import { Link} from "react-router-dom";
import { useEffect, useState } from "react";
import { fs_db } from "../../firebase/config";
import { collection, getDocs } from "firebase/firestore";

function NavBar() {
    const [categories, setCategories] = useState([]);
   
    useEffect(() => {
        const catCollection = collection(fs_db, "categories");

        getDocs(catCollection).then((snapshot) => {
            if(snapshot.size != 0) {
                const categoriesList = snapshot.docs.map((doc) => {return {id: doc.id, ...doc.data()}});
                setCategories(categoriesList);
            } else {
                console.log("Ups! No se encontraron categorias cargadas");
            }
        }).catch(error => {
            console.log.error(error);
        });
    
        return () => {

        }
    }, []);

    return(
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container px-4 px-lg-5">
                <Link to="/" className="navbar-brand">
                    <img src={ch_logo}  width={45} />
                </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <NavBarBtn path="/" text="Todos" />
                        {
                            categories.map((cat) => { return <NavBarBtn key={cat.id} text={cat.label} path={"/category/" + cat.name} />})
                        }
                    </ul>
                    <CartWidget />
                </div>
            </div>
        </nav>
    );
}

export default NavBar;