import { NavLink } from "react-router-dom";

function NavBarBtn({text, path}) {

    return(
        <li className="nav-item">
            <NavLink to={path} className="nav-link">{text}</NavLink>
        </li>
    );
}

export default NavBarBtn;