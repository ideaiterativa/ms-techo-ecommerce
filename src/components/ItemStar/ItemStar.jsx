const ItemStar = ({starsCount, displayOrientation}) => {
    const cant_stars = 5;
    let stars = new Array(cant_stars);

    for (let index = 1; index <= cant_stars; index++) {
        stars[index-1] = <div key={"star_" + index} className={"bi-star" + (index <= starsCount ? "-fill" : "")}></div>
    }

    return(
        <div className={"d-flex justify-content-"+ (displayOrientation != undefined ? displayOrientation : "center") +" small text-warning mb-2"}>
            {
                stars.map((e) => {return e})
            }
        </div>
    );
}

export default ItemStar;