import { Link } from "react-router-dom";

const CheckoutButton = () => {
    return(
        <Link to={"/checkout"}>
            <button className="btn btn-primary btn-outline flex-shrink-0" type="button">
                <i className="bi bi-cart-check-fill me-1"></i>Terminar Compra
            </button>
        </Link>
    );
}

export default CheckoutButton