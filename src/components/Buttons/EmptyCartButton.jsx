const EmptyCartButton = ({onEmptyCart}) => {
    return(
        <button className="btn btn-danger btn-outline flex-shrink-0 add-control" type="button" onClick={onEmptyCart}>
            <i className="bi bi-cart-x-fill me-1"></i>Vaciar carrito
        </button>
    );
}

export default EmptyCartButton