import {Link} from 'react-router-dom'

const GoToCartButton = () => {
    return(
        <Link to="/cart">
            <button className="btn btn-secondary btn-outline flex-shrink-0" type="button">
                <i className="bi-cart-fill me-1"></i>Ver el carrito
            </button>
        </Link>
    )
}

export default GoToCartButton;