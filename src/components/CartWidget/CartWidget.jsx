import { Link } from "react-router-dom";
import { useCartContext } from "../../context/CartContext";

function CartWidget() {
    const {totalProducts} = useCartContext();

    return (
        <Link to="/cart">
            <button className="btn btn-outline-dark" type="submit">
                <i className="bi-cart-fill me-1"></i>
                Cart
                <span className="badge bg-dark text-white ms-1 rounded-pill">{totalProducts}</span>
            </button>
        </Link>
    );
}

export default CartWidget;