import { useState } from "react";
import './ItemCount.css';

function ItemCount({cantInicial, stock, addToCart}) {
    const [cant, setCant] = useState(cantInicial);

    const incrementar = () => {
        if(cant < stock) {
            setCant(cant+1);
        }
    }

    const decrementar = () => {
        if(cant > cantInicial) {
            setCant(cant-1);
        }
    }

    return(
        <>
            <div className="d-flex">
                <button className="btn btn-outline-dark flex-shrink-0" type="button" onClick={() => decrementar()}>-</button>
                <input id="inputQuantity" className="form-control text-center me-3 cant-control" type="text" value={cant} readOnly />
                <button className="btn btn-outline-dark flex-shrink-0" type="button" onClick={() => incrementar()}>+</button>
            </div>
            <div className="d-flex">
                <button className="btn btn-secondary btn-outline flex-shrink-0 add-control" type="button" onClick={() => {addToCart(cant)}} disabled={!stock}>
                    <i className="bi-cart-fill me-1"></i>Add to cart
                </button>
            </div>
        </>
    );
}

export default ItemCount;