import Item from "../Item/Item";

function ItemList({items}) {
    return(
        <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            {
                (
                    items.length > 0 ?
                    items.map((prod) => {
                        return <Item key={prod.sku} id={prod.id} name={prod.name} countStars={prod.stars} price={prod.price} imgSource={prod.img_source}/>
                    }) :
                    <h2>No hay productos</h2>
                )
            }
        </div>
    );
}

export default ItemList;