import { useState } from "react";
import ItemCount from "../ItemCount/ItemCount";
import ItemStar from "../ItemStar/ItemStar";
import GoToCartButton from "../Buttons/GoToCartButton";
import { useCartContext } from "../../context/CartContext";

const ItemDetail = ({product}) => {
    const [cantidad, setCantidad] = useState(0);
    const {addProduct} = useCartContext();

    const handleAddToCart = (cant) => {
        setCantidad(cant);
        addProduct(product, cant);
    }

    return(
        <div className="row gx-4 gx-lg-5 align-items-center">
            <div className="col-md-6"><img className="card-img-top mb-5 mb-md-0" src={"/img/products/" + product.img_source} alt={product.name} /></div>
            <div className="col-md-6">
                <ItemStar starsCount={product.stars} displayOrientation="left" />
                <div className="small mb-1">SKU: {product.sku}</div>
                <h1 className="display-5 fw-bolder">{product.name}</h1>
                <div className="fs-5 mb-5">
                    {(product.oldPrice != undefined ? <span className="text-decoration-line-through">$&nbsp;{product.oldPrice}</span> : "")}
                    &nbsp;
                    <span>$&nbsp;{product.price}</span>
                </div>
                <p className="lead">{product.description}</p>
                <div className="d-flex">
                    {
                        (cantidad > 0 ?  <GoToCartButton /> : <ItemCount cantInicial={1} stock={product.stock} addToCart={handleAddToCart}></ItemCount>)
                    }
                </div>
            </div>
        </div>
    )
}

export default ItemDetail;