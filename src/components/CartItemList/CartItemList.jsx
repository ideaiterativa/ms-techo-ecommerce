import { Link } from "react-router-dom";

const CartItemList = ({item, cantidad, onClick}) => {
    return(
        <tr>
            <td>{item.sku}</td>
            <td>
                <Link to={"/item/"+item.id}>{item.name}</Link>
            </td>
            <td>$&nbsp;{item.price}</td>
            <td className="text-center">{cantidad}</td>
            <td className="text-center">$&nbsp;{item.price * cantidad}</td>
            <td className="text-end">
                <button className="btn btn-secondary btn-outline flex-shrink-0 add-control" type="button" onClick={() => onClick(item.id)}>
                    <i className="bi bi-trash-fill"></i>Quitar
                </button>
            </td>
        </tr>
    );
}

export default CartItemList;