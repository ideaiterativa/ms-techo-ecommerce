import { useEffect, useState } from "react";
import ItemList from "../ItemList/ItemList";
import Loading from "../Loading/Loading";
import { useParams } from "react-router-dom";
import { fs_db } from "../../firebase/config";
import { collection, getDocs, query, where } from "firebase/firestore";

function ItemListContainer() {
    const {categoryId} = useParams();
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        let refCollection;
        const prodCollection = collection(fs_db, "products");

        if(categoryId) {
            refCollection = query(prodCollection, where("category", "==", categoryId));
        } else {
            refCollection = prodCollection;
        }

        getDocs(refCollection).then((snapshot) => {
            if(snapshot.size != 0) {
                const productList = snapshot.docs.map((doc) => {return {id: doc.id, ...doc.data()}});
                setProducts(productList);
            } else {
                setProducts([]);
            }

            setLoading(false);
        
        }).catch(error => {
            console.log.error(error);
            setProducts([]);
            setLoading(false);
        });
        
        return () => {

        }
    }, [categoryId]);

    return(
        (loading ? <Loading message="Cargando productos..." /> :
        <section className="py-5">
            <div className="container px-4 px-lg-5 mt-5">
                <ItemList items={products} />
            </div>
        </section>  
        )
    );
}

export default ItemListContainer