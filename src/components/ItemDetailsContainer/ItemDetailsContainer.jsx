import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Loading from "../Loading/Loading";
import ItemDetail from "../ItemDetail/ItemDetail";
import { doc, getDoc } from "firebase/firestore";
import { fs_db } from "../../firebase/config";

const ItemDetailsContainer = () => {
    const {id} = useParams();
    const [product, setProduct] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const prodRef = doc(fs_db, "products", id);

        getDoc(prodRef).then((snapshot) => {
            setLoading(false);

            if(snapshot.exists()) {
                setProduct({id: snapshot.id, ...snapshot.data()});
            } else {
                console.log("No hay producto");
            }

        }).catch(error => {
            console.log.error(error)
        });
        
        return () => {

        }
    }, [id]);

    return(
        (loading ? <Loading message="Cargando descripción del producto..." /> : 
        <section className="py-5">
            <div className="container px-4 px-lg-5 my-5">
                {
                 (product != null ? <ItemDetail product={product} /> : <h2>El producto no existe</h2>)
                }
            </div>
        </section>
        )
    );
}

export default ItemDetailsContainer