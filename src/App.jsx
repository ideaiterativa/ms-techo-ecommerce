import './App.css'
import Footer from './components/Footer/Footer'
import Header from './components/Header/Header'
import ItemDetailsContainer from './components/ItemDetailsContainer/ItemDetailsContainer'
import ItemListContainer from './components/ItemListContainer/ItemListContainer'
import NavBar from './components/NavBar/NavBar'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import CartProvider from './context/CartContext'
import CartListContainer from './components/CartListContainer/CartListContainer'
import Checkout from './components/Checkout/Checkout'


function App() {

  return (
    <CartProvider>
      <BrowserRouter>
        <NavBar />
        <Header />
        
        <Routes>
          <Route path='/' element={<ItemListContainer />} />
          <Route path='/category/:categoryId' element={<ItemListContainer />} />
          <Route path='/item/:id' element={<ItemDetailsContainer />} />
          <Route path='/cart' element={<CartListContainer />} />
          <Route path='/checkout' element={<Checkout />} /> 
          <Route path='*' element={<h1>404</h1>} />
        </Routes>

        <Footer />
      
      </BrowserRouter>
    </CartProvider>
  )
}

export default App
